@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['route' => 'survey.answers', 'files' => true, 'id' => 'survey']) !!}
        <input type="hidden" name="questions" value="{{count($result)}}">
        @foreach($result as $key=>$value)
            <div class="my-3 p-3 bg-white rounded shadow-sm">
                <h6 class="border-bottom border-gray pb-2 mb-0">{{$value->question}}</h6>
                <input type="hidden" name="question_{{$key}}" value="{{$value->id}}">
                @foreach($value->survey_answers as $answer)
                    <div class="media text-muted pt-3">
                        <input type="radio" id="survey_radio" value="{{$answer->id}}" name="survey_radio_{{$key}}">
                        <label for="survey_radio">{{$answer->answer}}</label>
                    </div>
                @endforeach
            </div>
        @endforeach
        <button type="submit" class="btn btn-primary">Submit</button>
        {!! Form::close() !!}
    </div>
    {{--    </div>--}}
@stop
@section('after-scripts-end')

@endsection
