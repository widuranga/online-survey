@extends('layouts.app')

@section('content')
    <div class="container">

        {!! Form::open(['route' => 'survey.store', 'files' => true, 'id' => 'survey']) !!}
        <div class="row">
            <div class="col-md-6 mb-3 {{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::text('Email','',['name'=>'email','class'=>'form-control']) !!}
                @if ($errors->has('email'))
                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                @endif
            </div>
            <button class="col-md-6 mb-5 btn btn-primary" type="submit">Submit</button>
        </div>
        {!! Form::close() !!}

    </div>
@endsection
