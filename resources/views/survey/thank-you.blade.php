@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="jumbotron text-center">
            <h1 class="display-3">Thank You!</h1>
            <hr>
            <p class="lead">
                <a class="btn btn-primary btn-sm" href="{{route('survey.create')}}" role="button">Continue to homepage</a>
            </p>
        </div>
    </div>
@endsection
