@extends('layouts.app')

@section('after-style-end')
    <style>
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        #question_1 {
            width: 100%;
            height: 400px;
        }

        #question_2 {
            width: 100%;
            height: 400px;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    welcome to admin
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="question_1"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="question_2"></div>
                    </div>
                </div>
                <div class="row input-daterange">
                    <div class="col-md-4">
                        <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date"
                               readonly/>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date"
                               readonly/>
                    </div>
                    <div class="col-md-4">
                        <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                        <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" id="order_table">
                        <thead>
                        <tr>
                            <th>Email</th>
                            <th>Question-01</th>
                            <th>Question-02</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>
@stop
@section('after-scripts-end')
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script>
        $(document).ready(function () {

            var chart_1 = am4core.create("question_1", am4charts.PieChart);
            var chart_2 = am4core.create("question_2", am4charts.PieChart);

            chart_1.dataSource.url = "{{route('admin.chart-data',1)}}";
            chart_2.dataSource.url = "{{route('admin.chart-data',2)}}";

            // Add and configure Series
            var pieSeries_1 = chart_1.series.push(new am4charts.PieSeries());
            var pieSeries_2 = chart_2.series.push(new am4charts.PieSeries());
            pieSeries_1.dataFields.value = "answer_count";
            pieSeries_1.dataFields.category = "answer";

            pieSeries_2.dataFields.value = "answer_count";
            pieSeries_2.dataFields.category = "answer";


            $('.input-daterange').datepicker({
                todayBtn: 'linked',
                format: 'yyyy-mm-dd',
                autoclose: true
            });

            load_data();

            function load_data(from_date = '', to_date = '') {
                $('#order_table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: '{{ route("admin.data") }}',
                        data: {from_date: from_date, to_date: to_date}
                    },
                    columns: [
                        {
                            data: 'email',
                            name: 'email'
                        },
                        {
                            data: 'q_1',
                            name: 'q_1'
                        },
                        {
                            data: 'q_2',
                            name: 'q_2'
                        }
                    ]
                });
            }

            $('#filter').click(function () {
                var from_date = $('#from_date').val();
                var to_date = $('#to_date').val();
                if (from_date != '' && to_date != '') {
                    $('#order_table').DataTable().destroy();
                    load_data(from_date, to_date);
                } else {
                    alert('Both Date is required');
                }
            });

            $('#refresh').click(function () {
                $('#from_date').val('');
                $('#to_date').val('');
                $('#order_table').DataTable().destroy();
                load_data();
            });

        });
    </script>
@endsection
