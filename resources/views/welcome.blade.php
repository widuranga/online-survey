@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    welcome to online survey
                </div>
                <div class="links">
                    <a href="{{route('survey.create')}}">Start Now</a>
                </div>
            </div>
        </div>

    </div>
@stop
@section('after-scripts-end')

@endsection
