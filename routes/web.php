<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SurveyUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('survey', 'SurveyUserController')->only( 'create', 'store');
Route::get('survey/questions', [SurveyUserController::class, 'questions'])->name('survey.questions');
Route::post('survey/answers', [SurveyUserController::class, 'answers'])->name('survey.answers');
Route::get('survey/thank-you', [SurveyUserController::class, 'thanks'])->name('survey.thank-you');



Route::group(['middleware' => ['web']], function () {
    Route::group([
        'prefix' => 'admin'
    ],
        function () {
            /**
             * GET Routes
             */
            Route::get('/', [
                'as' => 'admin.index',
                'uses' => 'AdminController@index'
            ]);

            Route::get('data', [
                'as' => 'admin.data',
                'uses' => 'AdminController@data'
            ]);

            Route::get('chart-data/{id}', [
                'as' => 'admin.chart-data',
                'uses' => 'AdminController@chartData'
            ]);

            Route::get('chart-data-2', [
                'as' => 'admin.chart-data-2',
                'uses' => 'AdminController@chartData2'
            ]);





            /**
             * POST Routes
             */


        });
});
