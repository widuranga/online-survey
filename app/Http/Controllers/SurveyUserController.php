<?php

namespace App\Http\Controllers;

use App\SurveyQuestion;
use App\SurveyResult;
use App\SurveyUser;
use Illuminate\Http\Request;
use App\Http\Requests\SurveyUserRequest;
use Illuminate\Support\Facades\DB;

class SurveyUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('survey.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(SurveyUserRequest $request)
    {
        DB::beginTransaction();
        try {
            $SurveyUser = SurveyUser::create([
                'email' => $request->get('email'),
                'status' => '1'
            ]);
            DB::commit();
            $request->session()->forget('survey-users');
            $request->session()->put('survey-users', $SurveyUser->id);
            return redirect()->route('survey.questions');
        } catch (\Exception $exception) {
            DB::rollback();
            return redirect()->route('survey.create')->withInput();
        }


    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function questions(Request $request)
    {
        if ($request->session()->has('survey-users')) {
            $result = SurveyQuestion::with('survey_answers')->get();
            return view('survey.show-questions')->withResult($result);
        }else{
            return redirect()->route('survey.create');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\SurveyUser $surverUser
     * @return \Illuminate\Http\Response
     */
    public function answers(Request $request)
    {
        DB::beginTransaction();
        try {
            for ($x = 0; $x < $request->questions; $x++) {
                SurveyResult::create([
                    'survey_user_id'=>$request->session()->get('survey-users'),
                    'survey_question_id'=>$request->get('question_'.$x),
                    'survey_answer_id'=>$request->get('survey_radio_'.$x),
                ]);
            }
            DB::commit();
            $request->session()->forget('survey-users');
            return redirect()->route('survey.thank-you');
        } catch (\Exception $exception) {
            DB::rollback();
            return $exception->getMessage();
            return redirect()->route('survey.questions')->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function thanks()
    {
        return view('survey.thank-you');
    }

}
