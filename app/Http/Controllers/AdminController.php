<?php

namespace App\Http\Controllers;

use App\SurveyResult;
use App\SurveyUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function data(Request $request)
    {
        if (request()->ajax()) {
            $user_ = SurveyUser::whereIn('id', function ($query) {
                $query->select('survey_user_id')->from('survey_results');
            });
            if (!empty($request->from_date)) {
                $user_->whereBetween('created_at', array($request->from_date, $request->to_date));
            }
            $user = $user_->get();
            $result = collect();
            foreach ($user as $value) {
                $tt = collect();
                $tt->put('email', $value->email);
                $data = SurveyResult::with('answer')->where(['survey_user_id' => $value->id])->get();
                foreach ($data as $val) {
                    $tt->put('q_' . $val->survey_question_id, $val->answer->answer);
                }
                $result->push($tt);

            }
            return datatables()->of($result)->make(true);

        }
    }


    public function chartData($id)
    {

       return $result = DB::table('survey_results')
            ->join('survey_answers', 'survey_results.survey_answer_id', '=', 'survey_answers.id')
            ->select(DB::raw('count(survey_results.survey_answer_id) as answer_count,survey_answers.answer'))
            ->where('survey_results.survey_question_id', '=', $id)
            ->groupBy('survey_results.survey_answer_id')
            ->get();

    }
}
