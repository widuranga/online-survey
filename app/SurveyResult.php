<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurveyResult extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'survey_user_id',
        'survey_question_id',
        'survey_answer_id',
    ];

    public function answer()
    {
        return $this->belongsTo(SurveyAnswers::class,'survey_answer_id');
    }



}
