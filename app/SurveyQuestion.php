<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyQuestion extends Model
{
    //
    public function survey_answers()
    {
        return $this->hasMany(SurveyAnswers::class);
    }
}
